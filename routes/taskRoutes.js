const express = require("express");
// express.Router() method allows access to HTTP methods
const router = express.Router();
const taskController = require("../controllers/taskController");

// model>>controller>>routes>>application
// Create - task routes
router.post("/addTask", taskController.createTaskController);

// Get all tasks
router.get("/allTasks", taskController.getAllTasksController);

router.delete("/deleteTask/:taskId", taskController.deleteTaskController)

router.put("/archive/:id", (req, res) => {
  taskController.changeNameToArchive(req.params.id).then(resultFromController => res.send(resultFromController));
})

router.patch("/updateTask/:taskId", taskController.updateTaskNameController)

router.get("/getName/:taskId", taskController.getSpecificTaskName);



// sender of value on the function for invocation = argument
// receiver of value = parameter


// Activity s36

/*
  [GET -Wildcard required]
    1. Create a controller function for retrieving a specific task.
    2. Create a route 
    3. Return the result back to the client/Postman.
    4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
  NOTE: Screenshot your postman interface and mongodb collection results as proof of successful request and response

  [PUT - Update - Wildcard required]
    5. Create a controller function for changing the status of a task to "complete".
    6. Create a route
    7. Return the result back to the client/Postman.
    8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.

  NOTE: Screenshot your postman interface and mongodb collection results as proof of successful request and response
  9. Create a git repository named S31.
  10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
  11. Add the link in Boodle.

*/

router.get("/findSpecificTask/:taskId", taskController.getSpecificTask);

router.put("/changeSpecificTask/:taskId", taskController.updateSpecificTask)

module.exports = router;


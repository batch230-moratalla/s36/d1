/*
	Why do we have to?
	- Better code readability
	- Improve scalabiliuty
	- Better code maintainability
	What do we separate
	- Models
	- controllers - kung san ung mga function and process ng mga programs
	- Routes - end point
	Models = Contain what objects are needed in our API
		- ex = users, courses
		- Object schemas and relationshops are defines here
		- Schemas are JSON object that define the structure and content of documents
	Controllers = Contains instruction HOW your API will perform its intended task
		- Mongoose models queries are used here ex
		- Model.find()
		- Model.findByIdAndDelete()
		- // https://mongoosejs.com/docs/queries.html link that usually code that we used
	Routes = Defines When particular controller will be used
		- A specific controller action will be called when a specific HTTP method is received on a specific API endpoint
*/

/*
	Preparations:
	npm init -y
	npm install express
	npm install mongoose
*/

// mongodb+srv://admin:admin@batch230.1dtuoqz.mongodb.net/s36?retryWrites=true&w=majority"

// index.js >> routes >> controllers >> models
// require the installed modules
const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require("./routes/taskRoutes");

// port
const port = 4000;

// server
const app = express();

// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@batch230.1dtuoqz.mongodb.net/s36?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));

// middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Routes Grouping - organize the access for each resources.

app.use("/tasks", taskRoutes); //localhost:4000/tasks/

// port listener
app.listen(port, () => console.log(`Server is runnig at port ${port}`))